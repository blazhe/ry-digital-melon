# Eversheds Sutherland

These guidlines, principles and standards allow us to:

- Produce code of a consistent quality across all projects we undertake
- Work concurrently with multiple devs on the same codebase at the same time in the same way
- Produce code that is less prone to bugs and regressions, is easier to understand and debug
- Write code that supports re-usebailty.

It is required reading for all FE devs (internal and external) working on any RY development projects.

# Ways of working

Branching structure we use is [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow), please read this link if unfamilair.
All work to be branched off of `develop` and Pull requested back into `develop`.
Naming convention for feature branches: `feature/ticket-number` or `feature/feature-name`.

# Key principles

## Performance

Page load times (both real and perceived) are a key consideration for users of all browsers and device types.
There are some general things we can do in front-end development:

- Send fewest bytes possible down the wire
- Avoid unnecessary use of `display: none;`
- Keep CSS selectors concise (be wary of SASS nesting)
- Minimise HTTP requests
- Minimise blocking – content should be readable before client side processing

Lazy load 'supplementary' content (especially images)

## Don't Repeat Yourself (DRY)

If you repeat anything that has already been defined in code, refactor it so that it only ever has one representation in the codebase.

If you stick to this principle, you will ensure that you will only ever need to change one implementation of a feature without worrying about needing to change any other part of the code.

## Separation of concerns

Separate structure from presentation from behaviour to aid maintainability and understanding.

- Keep CSS (presentation), JS (behaviour) and HTML (structure) in separate files
- Avoid writing inline CSS or Javascript in HTML
- Avoid writing CSS or HTML in Javascript
- Don't choose HTML elements to imply style
- Where appropriate, use CSS rather than Javascript for animations and transitions
- Try to use templates when defining markup in Javascript

## Writing code that is readlable

> Debugging is twice as hard as writing the code in the first place. Therefore, if you write
> the code as cleverly as possible, you are, by definition, not smart enough to debug it. - Brian Kerninghan.

Hard to read or obscure code is difficult to maintain and debug. Don't be too clever; write code to be read.
Good reading - [Keep It Simple Stupid](https://en.wikipedia.org/wiki/KISS_principle)

## Commenting

It's not always easy to convey a particular architectural decision in code so make sure to comment it.

Someone should be able to pick up your code and instantly understand it.

Be verbose with your comments but ensure:

- Your comments add something to the code, they don't just repeat what is there
- They are kept up to date, if you change something that has been commented, ensure you up date the comment as well
- If code needs extensive commenting, can it be refactored to make it less complex / easier to understand?
- You focus on why rather than how - unless your code is too complex, it should be self documenting

Don't leave commented out chunks of code in the codebase. It makes the code look unfinished, and can be confusing for other developers.

# Styling with CSS and SASS

Our approach to CSS is influenced by Nicole Sullivan's OOCSS ideas, and Jonathan Snook's Scalable and Modular Architecture for CSS (SMACSS), both of which advocate a general separation of concerns to promote re-usability and prevent code bloat.

- Lint your SCSS according to the scss-lint configuration file found in the root of all cx projects
- Promote scalable and modular css architecture using the principles defined in the SMACSS style guide
- Utilise [BEM's](http://getbem.com/introduction/) 'Block', 'Element', 'Modifier' methodology
- Use classes rather than element selectors to de-couple CSS from HTML semantics and ensure that your code doesn't impact how it may be integrated into backend systems
- Make layouts fluid, using variable units of measurement
- Use id selectors only when explicitly required – they prohibit re-use, and may need to be re-written during systems integration
- Use short hex values where applicable, e.g. #fff instead of #ffffff
- Consider using @warn and @debug directives to aid development, especially within mixins

Use SCSS variables appropriately to ensure you code is kept DRY

```scss
// no
h3 {
  color: white;
}

// yes
$white: #fff;

h3 {
  color: $white;
}
```

Each selector and style declaration should be on its own line to help with Git diffs and error reporting.

```scss
// good
h3,
.gamma,
%gamma {
  @include font-size($h3-font-size);
  line-height: $heading-line-height;
}

// not so good
h3,
.gamma,
%gamma {
  @include font-size($h3-font-size);
  line-height: $heading-line-height;
}
```

- Don't specify units for zero values, e.g. margin: 0; instead of margin: 0px;
- Use 0 instead of none, e.g. border: 0; rather than border: none;
- Make sure you don't add prefixes to your properties as we use Autoprefixer to take care of this
- Wherever possible, specific page-level styling should be avoided in favour of layout or component modifiers.
- Avoid inline CSS.

- Use shorthand properties:

```scss
// no
margin: 1px 1px 1px 1px;

// yes
margin: 1px;

// no
margin: 0 0 20px 0;

// yes
margin: 0 0 20px;
```

It is worth noting that whilst the CSS generated by SASS can be outputted with optimisations applied, we cannot make assumptions about how the code we deliver will be used, and as such creating optimal optimal source code is preferred to relying on processors.

- Write colours in lowercase:

```scss
// no
$color: #1AB2C0

// yes
$color: #1ab2c0
```

- Omit protocols from external resources to prevent unintended security warnings through accidentally mixing protocols, and for small file size savings:

```scss
// nope
.main-navigation {
  background: url(http://random.com/images/image.jpg);
}

// yes
.main-navigation {
  background: url(//random.com/images/image.jpg);
}
```

Use whitespace to aid readability. Anything which modifies the context such as a selector or a media query should be preceded with a blank line.

```scss
// bad
.foo {
  color: blue;
  @media screen and (min-width: 1000px) {
    color: yellow;
  }
  .bar {
    color: red;
    @media screen and (min-width: 1000px) {
      color: green;
    }
  }
}

// good
.foo {
  color: blue;

  @media screen and (min-width: 1000px) {
    color: yellow;
  }

  .bar {
    color: red;

    @media screen and (min-width: 1000px) {
      color: green;
    }
  }
}
```

## Depth of Applicability

Don't over-specify CSS selectors. Overly specified selectors are difficult to understand and lead to subsequent selectors needing to be of an even higher specificity. (See SMACSS' [depth of applicability](http://smacss.com/book/applicability)):

```scss
// nope
#sidebar div ul > li {
  margin-bottom: 5px;
}
```

The above example is tightly coupled to the HTML structure which prevents re-use and is brittle - if the HTML needs changing, then the style will break.

## Over qualification

Don't attribute classes or ID's to elements

```scss
// over qualified id selector
ul#main-navigation {
  ...
}

// over qualified class selector
table.results {
  ...
}
```

As above, you will be binding site structure with presentation making the site harder to maintain and inhibit re-use.

## Commenting

Use comments for:

- Explain design or architectural decisions
- Dividing up groups of declarations using standard block and single line comment formats. (If you have too many major sections in your partial, perhaps it should be more than one partial!)

```scss
/**
 * This is a block comment
 * across multiple lines
 */

// Single line comment
```

Use multiline comments if you want the comment to be preserved in the compiled CSS.

```scss
/* This comment is
 * several lines long.
 * since it uses the CSS comment syntax,
 * it will appear in the CSS output. */
body {
  color: black;
}
```

## Unit sizing

Up for debate on this one, my preference is rem were possible with a pixel fallback. Font size mixin maybe

```scss
font-size: 12px;
font-size: 1.2rem; /* This line is ignored by IE6, 7 & 8 */
```

- Use unitless line-heights.
- Use percentages for fluid layouts and components.
- Use pixels to specify the following properties unless percentages make more sense (but as above, exercise good judgement): `margin`, `padding`, `top`, `right`, `bottom`, `left`.

```scss
//  This makes sense
.dropdown-toggle:hover > .dropdown-menu {
  display: block;
  top: 100%; // display just below dropdown-toggle
}

//  This doesn't (where has this number come from??)
.dropdown-toggle:hover > .dropdown-menu {
  display: block;
  top: 62px; // height of dropdown-toggle at present
}

// This makes sense
.box {
  margin-bottom: 20px;
}

//  This doesn't:
.box {
  margin-bottom: 6.33%; // too ambiguous
}
```

- Use box-sizing: `border-box` globally. This makes dealing with multiple units in this fashion a lot nicer.

# HTML Markup

- Use well-structured, semantic markup.
- Use double quotes on all attributes.
- Use soft tabs, 2 space indents.
- Ensure you write valid HTML. Check using tools such as the W3C Markup Validation Service.
- Do not omit optional tags. It may be unclear whether a tag has been deliberately omitted, or if it has been left out accidentally.
- Although unquoted attributes are supported, always quote attribute values.

```html
<input type="text" class="form__field" form__field--string />
<!-- uh oh -->
```

- Omit protocols from external resources to prevent unintended security warnings:

```html
<!-- Don't do -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<!-- Do -->
<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
```

### Doctype

```html
<!DOCTYPE html>
```

### Write semantic markup

```html
<!-- bad: -->
<div class="heading-main">My Blog</div>
<br /><br />
... content ...
<br /><br />

<!-- bad: -->
<h1>I want to draw attention to this as I am important</h1>
<h1>and so am I</h1>

<!-- good: -->
<h1>My Blog</h1>
<p>
  ... content ...
</p>
```

### Input labels

When using labels, try to use the wrapping pattern rather than the for attribute so we can avoid using ids which may interfere with integration with backend systems:

```html
<label>
  Address
  <input type="text" name="address" />
</label>

<label for="address">Address</label>
<input type="text" id="address" name="name" />
```

### Boolean attributes

The presence of the attribute itself implies that the value is "true", an absence implies a value of "false":

```html
<option selected="selected">value 1<option>
//vs
<option selected>value 1<option>
```

# Javascript

- Use soft-tabs with a two space indent
- Never use `eval`
- All projects will contain a `.jshintrc` file in the root. This will define the expected coding standards for the project, enforced by JSHint.
- Avoid applying styles with Javascript, preferably add or remove classes. Keep style in CSS to make it easier to maintain and debug.

```js
//  nope
<input type='text' style='display:none;' name='address_1' />

// yup
<input type="text" class="is-hidden" name="address_1" />
```

- Avoid inline Javascript in HTML markup.
- Remember to be DRY, Don't recreate functionality that may already be present in a utility library
- When using a third party library be sure to use `npm` to install it where possible
- Always use parentheses in blocks to aid readability:

```js
// worse
switch (action.type) {
  case 'SET_PROPERTY':
    let { key, value } = action.payload;
    return Object.assign({}, state, { [key]: value });
  case 'REMOVE_PROPERTY':
    let { key } = action.payload;
    state = Object.assign({}, state);
    delete state[key];
    return start;
}

// better
switch (action.type) {
  case 'SET_PROPERTY': {
    let { key, value } = action.payload;
    return Object.assign({}, state, { [key]: value });
  }
  case 'REMOVE_PROPERTY': {
    let { key } = action.payload;
    state = Object.assign({}, state);
    delete state[key];
    return start;
  }
}

// Or

// this good
if (true) {
  shuffle();
}

// not good...
if (true) shuffle();
```

- Use the `===` comparison operator to avoid having to deal with type coercion complications.
- Use quotation marks consistently, e.g. only use single or double quotes throughout your code, don't mix.

```js
// good...
let foo = 'Just a normal string';

// not good...
let foo = "<a href="/bar">HTML string with escaped double quotes";
```

- Don't use single vars

```js
// single var pattern
let myVar1 = 1,
  anotherVar2 = 'test',
  andAnotherVar = true;

// Urgh!
let a = 1,
  b = 2,
  sum = a + b,
  myobject = {},
  i,
  j;

// simple and easy to read
let myVar = 1;
let anotherVat2 = 'test';
let andAnotherVar = true;
```

- Avoid excessive function arguments
  If a function requires more than three arguments, considering refactoring to use a configuration object:

```js
// bad
const myFunction1 = (arg1, arg2, arg3, arg4) => {};

myFunction1('firstArgument', argument2, true, 'My Third Argument');

// good
const myFunction2 = (config) => {};

myFunction2({
  arg1: 'firstArgument',
  arg2: argument2
  arg3: true,
  arg4: 'My Third Argument'
})
```

## Naming conventions

## Commenting

```js
/**
 * Nicely formatted
 * multi-line comment block
 */
```

```js
/**
 * Event handler to close the menu
 * @param {Event} evt jQuery event object
 */
const myFunction = (evt) => {};
```

## Avoid HTML in JS

Long strings of text and HTML in JS are hard to maintain and understand. Considering using a template library such as Handlebars JS or mustache.

```js
// configuration object
templates: {
  'errorTemplate': '<div class="error-message"><p>Error message</p></div>'
}
```

Where possible, put templates in HTML or seperate files for a clear separation of behaviour and presentation:

```js
<script id="error-template" type="text/x-handlebars-template">
  <div class="error-message">
    <strong>The following errors have occurred:</strong>
    <p>{{error-message}}</p>
  </div>
</script>
```
