/**
 * NOTE: THIS IS A SAMPLE.
 * Any configuration to be merged with 'webpack.config.babel.js' SHOULD BE ADDED TO '/source/_app/webpack.app.js'.
 * Add new dependencies like so:
 * "yarn add autoprefixer import-glob-loader css-loader node-sass postcss-loader postcss-flexbugs-fixes mini-css-extract-plugin sass-loader style-loader --dev"
 * or
 * "npm install autoprefixer import-glob-loader css-loader node-sass postcss-loader postcss-flexbugs-fixes mini-css-extract-plugin sass-loader style-loader --save-dev"
 */

const webpack = require("webpack");
const path = require('path');
const { resolve } = require("path");
const globby = require("globby");
const plConfig = require("../../patternlab-config.json");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require('autoprefixer');

module.exports = env => {
  const app = {
    entry: {
      "css": globby.sync(
        [
          resolve(`${plConfig.paths.source.css}*.scss`),
        ], {
          gitignore: true
        }
      )
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "css/styles.css"
      })
    ],
    module: {
      rules: [{
        test: /\.scss$/,
        use: [{
            loader:(
              "style-loader",
              MiniCssExtractPlugin.loader
            )
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
              config: {
                path: 'postcss.config.js'
              }
            }
          },
          {
            loader: "sass-loader",
            options: {
              sassOptions: {
                sourceMap: true,
                outputStyle: 'compressed',
                indentWidth: 4,
                includePaths: [path.resolve(__dirname, 'TEST')],
              },
            }
          },
          {
            loader: "import-glob-loader"
          }
        ]
      }]
    }
  };
  return app;
};