const accordion = {
  init() {
    const accordionsWrapper = document.querySelectorAll('.accordion-wrapper');

    if (accordionsWrapper) {
      accordionsWrapper.forEach((entry, index) => {
        entry.classList.add('accordion-wrapper-' + index);

        const accordions = document.querySelectorAll(
          '.accordion-wrapper-' + index + ' .accordion'
        );

        const handleClick = accordion => () => {
          const prevState = accordion.selected;

          if (
            accordion.dataset.multiselect !== undefined &&
            accordion.dataset.multiselect === 'disabled'
          ) {
            [].forEach.call(accordions, accordion => {
              accordion.selected = false;
            });
          }

          accordion.selected = !prevState;

          [].forEach.call(accordions, accordion => {
            const container = accordion.querySelector('.accordion__container');

            if (accordion.selected) {
              container.style.height =
                accordion.querySelector('.accordion__body').offsetHeight + 'px';
              accordion.setAttribute('aria-expanded', true);
            } else {
              container.style.height = null;
              accordion.setAttribute('aria-expanded', false);
            }
          });
        };

        [].forEach.call(accordions, accordion => {
          accordion
            .querySelector('.accordion__head')
            .addEventListener('click', handleClick(accordion));

          const accordionHead = accordion.querySelector('.accordion__head');

          if (accordionHead.children.length) {
            accordionHead.parentNode.classList.add('accordion--with-image');
          }
        });
      });
    }
  }
};

export default accordion;
