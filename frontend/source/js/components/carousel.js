import Swiper from 'swiper';

const carousel = {
  init() {
    const carouselBulletText = document.querySelectorAll('.swiper-slide');
    const carouselRelatedText = document.querySelectorAll('.swiper-related')[0];

    let carouselBulletTextArr = [];

    function setRelatedTextMargin() {
      const carouselPaginationHeight = document.querySelectorAll(
        '.swiper-pagination'
      )[0];

      if (carouselPaginationHeight) {
        carouselRelatedText.style.bottom =
          carouselPaginationHeight.offsetHeight + 'px';
      }
    }

    window.onresize = function() {
      setRelatedTextMargin();
    };

    setTimeout(function() {
      setRelatedTextMargin();
    }, 500);

    for (let i = 0; i < carouselBulletText.length; i++) {
      carouselBulletTextArr.push(carouselBulletText[i].dataset.bullet);
    }

    new Swiper('.swiper-container', {
      direction: 'horizontal',
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: 'true',
        renderBullet: function(index, className) {
          return `<li class="${className}">0${index +
            1}<span class="swiper-pagination-bullet-text">${
            carouselBulletTextArr[index]
          }</span></li>`;
        }
      }
    });
  }
};

export default carousel;
