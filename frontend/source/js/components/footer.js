const footer = {
  init() {
    const footerAccordion = document.querySelectorAll('.footer-accordion');

    if (footerAccordion) {
      footerAccordion.forEach((entry, index) => {
        entry.classList.add('footer-accordion-' + index);

        const accordions = document.querySelectorAll(
          '.footer-accordion-' + index
        );

        const handleClick = accordion => () => {
          const prevState = accordion.selected;

          if (
            accordion.dataset.multiselect !== undefined &&
            accordion.dataset.multiselect === 'disabled'
          ) {
            [].forEach.call(accordions, accordion => {
              accordion.selected = false;
            });
          }

          accordion.selected = !prevState;

          [].forEach.call(accordions, accordion => {
            const container = accordion.querySelector(
              '.footer-accordion__container'
            );

            if (accordion.selected) {
              container.style.height =
                accordion.querySelector('.footer-accordion__list')
                  .offsetHeight + 'px';
              accordion.setAttribute('aria-expanded', true);
            } else {
              container.style.height = null;
              accordion.setAttribute('aria-expanded', false);
            }
          });
        };

        [].forEach.call(accordions, accordion => {
          accordion
            .querySelector('.footer-accordion__header')
            .addEventListener('click', handleClick(accordion));
        });
      });
    }
  }
};

export default footer;
