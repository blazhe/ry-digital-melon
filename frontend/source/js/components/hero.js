const hero = {
  init() {
    //const hero = document.querySelector('.people-profile .hero');
    const hero = document.querySelector('.main .hero');
    const searchBarHero = document.querySelector('.toolbar-search__standard');
    const searchBarBottom = document.querySelector('.toolbar-search__pop-up');
    const searchBarBottomInner = document.querySelector(
      '.toolbar-search__pop-up .outer-row'
    );
    const heroName = document.querySelector('.hero__headline');
    const heroPhone = document.querySelector('.hero__info-mobile');
    const heroEmail = document.querySelector('.hero__info-email');
    const heroInfo = document.querySelector('.toolbar-search__list');
    const heroStickyNav = document.querySelector('.header__sticky-area');

    if (hero) {
      let formContent = searchBarHero.innerHTML;
      //Clone hero content
      if (heroName) {
        let heroNameClone = heroName.cloneNode(true);
        heroStickyNav.appendChild(heroNameClone);
      }
      if (heroPhone) {
        let heroPhoneClone = heroPhone.cloneNode(true);
        heroStickyNav.appendChild(heroPhoneClone);
      }
      if (heroEmail) {
        let heroEmailClone = heroEmail.cloneNode(true);
        heroStickyNav.appendChild(heroEmailClone);
      }
      if (heroInfo) {
        let heroInfoClone = heroInfo.cloneNode(true);
        heroStickyNav.appendChild(heroInfoClone);
      }

      //Append to nav
      const heroPadding = 140;
      searchBarBottomInner.innerHTML = formContent;
      window.addEventListener('scroll', function() {
        if (window.scrollY > hero.offsetTop + hero.offsetHeight - heroPadding) {
          searchBarBottomInner.innerHTML = formContent;
          searchBarHero.innerHTML = '';
          searchBarBottom.classList.add('toolbar-search__pop-up--active');
          heroStickyNav.classList.add('header__sticky-area--active');
        } else {
          if (searchBarBottomInner.innerHTML === '') {
            // console.log('not full');
          } else {
            searchBarHero.innerHTML = formContent;
            searchBarBottomInner.innerHTML = '';
            searchBarBottom.classList.remove('toolbar-search__pop-up--active');
            heroStickyNav.classList.remove('header__sticky-area--active');
          }
        }
      });
    }
  }
};

export default hero;
