const nav = {
  events: {
    moveSearchBar: function() {
      const width =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;

      if (width < 960) {
        console.log('moveSearchBar sub 960');
      } else {
        console.log('moveSearchBar more 960');
      }
    }
  },

  init() {
    window.onresize = function() {
      nav.events.moveSearchBar();
    };
  }
};

export default nav;
