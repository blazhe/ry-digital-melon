import accordion from './components/accordion';
import cards from './components/cards';
import carousel from './components/carousel';
import hero from './components/hero';
import pagination from './components/pagination';
import tabs from './components/tabs';
//Polyfill for IE11/Edge for :focus-withih pseudo selector
import 'focus-within-polyfill';

const frontEndBuild = {
  init() {
    const root = document.documentElement;
    root.classList.remove('no-js');
    root.classList.add('js');
    accordion.init();
    cards.init();
    carousel.init();
    hero.init();
    pagination.init();
    tabs.init();
  }
};

frontEndBuild.init();
